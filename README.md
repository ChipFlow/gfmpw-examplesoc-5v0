# ChipFlow example SOC on gf180mcu with 5V version of c4m-flexcell

[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

This is the [example-socs](https://gitlab.com/ChipFlow/example-soc) design
P&R with 5V standard cells for GFMPW.
[pnr_socs repo](https://gitlab.com/ChipFlow/pnr_socs) contains the
open source flow to regenerate this gds file.
